import type { MutationTree } from 'vuex';
import { buildDict, parseUserJwt } from '../src/helpers.ts';
import type { User } from '../src/user.ts';

export interface RootState {
    token: string | null;
    user: User | null;
    preToken: string | null;
    spelling: string | null;
    highlightedMorpheme: string | null;
    darkMode: boolean;
    translationModeVisible: boolean;
    translationMode: boolean;
    translationChanges: Record<string, string>;
    adPlaceholdersVisible: boolean;
    reducedItems: boolean;
    accounts: Record<string, string>;
}

export const state: () => RootState = () => ({
    token: null,
    user: null,
    preToken: null,
    spelling: null,
    highlightedMorpheme: null,
    darkMode: false,
    translationModeVisible: false,
    translationMode: false,
    translationChanges: {},
    adPlaceholdersVisible: false,
    reducedItems: false,
    accounts: {},
});


export const mutations: MutationTree<RootState> = {
    setToken(state, token) {
        if (!token) {
            state.token = null;
            state.user = null;
            return;
        }

        const user = parseUserJwt(token) as User | null;

        if (user && user.mfaRequired) {
            state.preToken = token;
        }

        if (user && user.authenticated) {
            state.preToken = null;
            state.token = token;
            state.user = user;
            return;
        }

        state.token = null;
        state.user = null;
    },
    cancelMfa(state) {
        state.preToken = null;
    },
    setSpelling(state, spelling) {
        state.spelling = spelling;
    },
    highlightMorpheme(state, morpheme) {
        state.highlightedMorpheme = morpheme;
    },
    setDarkMode(state, isDark) {
        state.darkMode = isDark;
    },
    showTranslationMode(state) {
        state.translationModeVisible = true;
    },
    translationInit(state) {
        state.translationMode = true;
    },
    translationCommit(state) {
        state.translationMode = false;
        state.translationChanges = {};
    },
    translationAbort(state) {
        state.translationMode = false;
        state.translationChanges = {};
    },
    translationPause(state) {
        state.translationMode = false;
    },
    translate(state, { translator, key, newValue }) {
        if (newValue !== translator.get(key)) {
            const translationChanges = { ...state.translationChanges };
            translationChanges[key] = newValue;
            state.translationChanges = translationChanges;
        } else {
            state.translationChanges = buildDict(function* (that) {
                for (const k in that) {
                    if (!that.hasOwnProperty(k)) {
                        continue;
                    }
                    if (k !== key) {
                        yield [k, that[k]];
                    }
                }
            }, state.translationChanges);
        }
    },
    restoreTranslations(state, translations) {
        if (translations) {
            state.translationMode = true;
            state.translationChanges = translations;
        } else {
            state.translationMode = false;
            state.translationChanges = {};
        }
    },
    toggleAdPlaceholdersVisible(state) {
        state.adPlaceholdersVisible = !state.adPlaceholdersVisible;
    },
    setReducedItems(state, value) {
        state.reducedItems = value;
    },
    setAccounts(state, accounts) {
        state.accounts = accounts;
    },
};
