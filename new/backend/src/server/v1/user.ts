import { ApiError, localeSpecific, replyError } from "#self/server/v1";
import { db } from "#self/db";
import { Type } from "@sinclair/typebox";
import { validateCaptcha } from "#self/util/turnstile";
import { isValidEmail, isValidUsername } from "@pronounspage/common/util/filters";

function isSpamLike(usernameOrEmail: string) {
    return usernameOrEmail.length > 128;
}

export const plugin = async function (app) {
    app.post(
        "/:locale/user/init",
        {
            schema: {
                body: Type.Object({
                    usernameOrEmail: Type.String(),
                    captchaToken: Type.String(),
                }),
            },
        },
        async (req, reply) => {
            let identifier = req.body.usernameOrEmail;
            if (isSpamLike(identifier)) {
                // This is different from how the old code does it
                // The old code does `req.socket.end()` (equivalent is `req.raw.socket.end()`)
                return replyError(reply, ApiError.BAD_REQUEST);
            }

            const isEmail = isValidEmail(identifier);
            const isUsername = isValidUsername(identifier);
            if (!isEmail || !isUsername) {
                return replyError(reply, ApiError.BAD_REQUEST); // TODO: Different error for this stuff
            }

            // We validate captcha after because it's considerably less time-consuming hypothetically
            // This requires an entire request
            if (!(await validateCaptcha(req.body.captchaToken))) {
                return replyError(reply, ApiError.BAD_REQUEST); // Yet another difference: old code has a separate error for captcha failures
            }


        }
    );
} satisfies AppPluginAsync;
export default plugin;
