import { PrismaClient } from "#prisma";
export type * from "#prisma";

export const db = new PrismaClient();
