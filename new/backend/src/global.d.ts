import type {
    FastifyInstance,
    FastifyPluginAsync,
    FastifyPluginOptions,
    FastifyReply,
    RawReplyDefaultExpression,
    RawRequestDefaultExpression,
    RawServerDefault,
} from "fastify";
import type { TypeBoxTypeProvider } from "@fastify/type-provider-typebox";
import type { log } from "#self/log";

type RawServer = RawServerDefault;
type RequestExpr = RawRequestDefaultExpression<RawServer>;
type ReplyExpr = RawReplyDefaultExpression<RawServer>;
type Log = typeof log;
type TypeProvider = TypeBoxTypeProvider;

declare global {
    type AppInstance = FastifyInstance<
        RawServer,
        RequestExpr,
        ReplyExpr,
        typeof log,
        TypeProvider
    >;
    type AppPluginAsync<
        Options extends FastifyPluginOptions = Record<never, never>,
    > = FastifyPluginAsync<Options, RawServer, TypeProvider, Log>;
    type AppReply = FastifyReply<RawServer, RequestExpr, ReplyExpr>;
}
