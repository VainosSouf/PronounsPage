export function identity<T>(value: T): T {
    return value;
}

// Generally, don't use this function to define commonly used functions.
// Prefer to write a manual implementation.
export function not<Args extends Array<unknown>>(
    f: (...args: Args) => boolean
): (...args: Args) => boolean {
    return (...args: Args) => !f(...args);
}
export function isNotNull<T>(value: T | null | undefined): value is T {
    return value != null;
}
export const isNull = not(isNotNull); // I know I said not to do this, but it's not used yet *however* it irks me to not have completeness.
export function isNotBlank(value: string | null | undefined): boolean {
    if (!isNotNull(value)) {
        return false;
    }
    if (value.length < 1) {
        return false;
    }
    return value.trim().length > 0;
}
export const isBlank = not(isNotBlank);

// This is unnecessarily typed but I don't really care, it works doesn't it?
export function toLowerCase<S extends string>(value: S): Lowercase<S> {
    return value.toLowerCase() as Lowercase<S>;
}

export function parseInteger(value: string, radix?: number): number {
    if (radix != null && (radix < 2 || radix > 36)) {
        throw new Error(
            `invalid radix ${radix} - must be between 2 and 36 (inclusive)`
        );
    }
    const parsed = parseInt(value, radix);
    if (isNaN(parsed)) {
        throw new Error("parsed value is NaN");
    }
    return parsed;
}

export function parseBool(value: unknown): boolean {
    switch (typeof value) {
        case "boolean":
            return value;
        case "string":
            value = value.trim().toLowerCase();
            if (value === "true" || value === "1" || value === "yes") {
                return true;
            }
            if (value === "false" || value === "0" || value === "no") {
                return false;
            }
            throw new Error(`unknown boolean constant: ${value}`);
        case "number":
            return value != 0;
        default:
            throw new Error(`Cannot convert value ${value} to a boolean`);
    }
}

export function capitalizeFirstLetter(value: string) {
    return value.charAt(0).toUpperCase() + value.substring(1);
}

// TODO: Benchmark this function with different implementations.
export function compileTemplate(
    template: string,
    values: Record<string, string>
): string {
    let string = "";
    let key = null;
    for (let i = 0; i < template.length; i++) {
        const c = template.charAt(i);
        if (key === null) {
            switch (c) {
                case "{":
                    key = "";
                    continue;
                default:
                    string += c;
                    break;
            }
        } else {
            switch (c) {
                case "}":
                    string += values[key];
                    key = null;
                    continue;
                default:
                    key += c;
                    break;
            }
        }
    }
    /*for (const key in values) {
        const value = values[key];
        string = string.replace(`{${key}}`, value);
    }*/
    return string;
}
