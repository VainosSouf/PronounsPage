# Zaimki.pl / Pronouns.page

## Dependencies

- [NodeJS](https://nodejs.org/en) version 18.17.1
- [Yarn](https://yarnpkg.com/getting-started/install) or other package manager

## Installation

```
git clone git@gitlab.com:PronounsPage/PronounsPage.git
```

We're using FontAwesome Pro, so to set up a local copy without having a FA license,
open `package.json` and replace

```
"@fortawesome/fontawesome-pro": "git+ssh://git@gitlab.com:Avris/FontAwesomePro.git",
```

with

```
"@fortawesome/fontawesome-pro": "git+ssh://git@gitlab.com:Avris/FakeFontAwesomePro.git",
```
or, for Git via HTTPS:

```
"@fortawesome/fontawesome-pro": "git+https://git@gitlab.com/Avris/FakeFontAwesomePro.git",
```

Do not commit that change!

## Build Setup

```bash
# install dependencies
$ make install

# configure environment
$ nano .env
$ make switch LANG=pl

# run unit tests
$ make test

# serve with hot reload at localhost:3000
$ make run

# build for production and launch server
$ make deploy
$ nuxt start
```

## Copyright

See [LICENSE](./LICENSE.md).

## Development Documentation

### User profiles

In order to test the login system, you can type in an email followed by a + (example@pronouns.page+). This way, you won't need a mailer, as the code will always be 999999. This feature is exclusive to development builds.

If you need an admin user in the development server, you can manually open the `db.sqlite` database
and give a user the `*` role in the `roles` column.

### Package manager

If you're having problems using Yarn, npm is probably fine, but remember to switch any `yarn [x]` commands for `npm run [x]`.\
Remember to modify the `.env` file. You don't really need to set up any external APIs, just make up a secret.
```text
// ...
SECRET=replaceThis
// ...
```

### Editor

#### Visual Studio Code
Recommended extensions are provided in `.vscode/extensions.json`
and Visual Studio Code will ask for your permission to install them upon project open.
`.vscode/settings.json` already includes settings to integrate with tooling.

#### JetBrains IDE (WebStorm, PhpStorm, …)
Most of the tooling is already shipped with a JetBrains IDE.
`.idea` already contains some settings to integrate with the tooling.
If you want syntax highlighting for `.suml` files, you need to manually bind them to `yaml` syntax
via `Settings › Editor › File Types`.

#### Others
When you want to use a different editor, you may need to configure some points to reduce manual work:

- Integrating ESLint with auto-fixing on save (the formatting relies on it)
- Integrating Jest to easily view test results
- Use `yaml` syntax highlighting for `.suml` files

### Tools

For frontend development, check out the [Vue Devtools](https://vuejs.org/guide/scaling-up/tooling.html#browser-devtools).

### Linting

The configured ESLint rules are most of the times the recommended presets, with some tweaks and formatting rules added.
If you encounter warnings, please try to fix them in new code, and make a sensible decision when touching old code.
If you encounter a rule you disagree on or think another would add benefit,
please make a remark in the MR / on Discord.

### Testing

We currently have Jest as a simple unit testing framework.
While we don’t require unit tests for new code, a well-designed test is greatly appreciated.
The coverage reports on the MR are only meant for information purposes, not as a call to cover lines at all costs.

### Pipelines

While as an Open Source project we don’t practice Continous Integration or Continous Delivery by the book,
we have a “CI/CD” pipeline to ensure that our tests are passing and the lints don’t produce errors or fixable warnings.
We use our VPS as a GitLab runner.
Deployment is done manually.

### Windows

Current setup depends on working symlinks, which means that an NTFS filesystem is unfortunately required for Windows at the moment.

If you encounter a `ParseError`, make sure the file is in `\n` (_Line Feed_) mode instead of `\r\n` (_Carriage Return Line Feed_).

If you want to automate the change, [try this](https://stackoverflow.com/a/50006730). \
Make sure that the `.editorconfig` looks like this:
```text
[*.suml]
end_of_line = lf
```

### Server Password

For unpublished locales and for the test server, a password is required to access the web interface.
If you're on our Discord, you can ask for it there (or just use `published: true` for local development).

### Review Checklist

A non-comprehensive list of things to look out during development and review:

- Meet accessibility criteria in frontend (contrasts at least AA)
- Be consistent with the design guidelines (at `/design`)
- Check frontend in both dark and light mode
- Check that changes work in a differently configured locale (especially for optionally configurable features)
- Add new translations to `_base/translations.suml` and if applicable add a condition to `missingTranslations.js`
- Consider backwards-compatibility (e.g. links to pronouns in their profile or mail signature shouldn’t break)
- If you changed code related to those scripts, check that they keep running:
  - `server/analyseLinks.js`
  - `server/calendar.js`
  - `server/calendarBot.js` (with arguments `en,pl,es,pt,de,nl,fr,ja,ru,sv,lad,ua,vi twitter,mastodon`)
  - `server/cards.js`
  - `server/cleanupAccounts.js`
  - `server/cleanupImages.js`
  - `server/miastamaszerujace.js` (pl locale only)
  - `server/migrate.ts`
  - `server/notify.js`
  - `server/stats.ts`
  - `server/subscriptions.js`

### Integrations

#### AWS

If you want to test with AWS, you can fill in all the `AWS_*` values with your own credentials in `.env.dist`.
For small changes, you can ask to have your branch deployed on a test server.

#### Emails

When using `NODE_ENV=development`, emails will be displayed in stdout without being sent.
It's not in an easily readable format, but for many cases good enough.
If you want to actually see the message in a mail client,
you need to specify `MAILER_TRANSPORT` to use any provider you want (on production SMTP via AWS is used).
Remember to also set `MAILER_OVERWRITE` to your own address
– this way you won't accidentally send anything out to random addresses.

### Sentry

To enable sentry, you have to set the corresponding environment variables.
Remember that releases will not be published for development builds,
so to test them, you have to build a production build (`make deploy` and `nuxt start`, both with `NODE_ENV=development`).

### Troubleshooting

#### Hot Module Replacement in Nuxt development mode
If you're having issues with [HMR](https://webpack.js.org/concepts/hot-module-replacement/) not reloading automatically:
- The modules within `package.json` are matching the repo's `package.json`.
- `nuxt.config.ts` has the following option:
  ```js
  watchers: {
      webpack: {
          aggregateTimeout: 300,
          poll: 1000
      }
  }
  ```

#### Out-of-Memory in Nuxt development mode
If you experience a `JavaScript heap out of memory` error, it may help to disable typechecking (you can still typecheck via your editor or `yarn vue-ts`):
- add to `nuxt.config.ts`:
  ```js
  typescript: {
      typeCheck: false,
  },
  ```

#### `ts-node` on another Node version
If you use another node version and run into an error when using `yarn ts-node` (e.g. `Unknown file extension ".ts"`), then either use the recommended node version or use `ts-node` via an experimental loader:
`node --loader ts-node/esm`.

## Translation

### Contributing to an existing language version

You can either translate on-site via the translation mode, which will get merged eventually by the team
or by directly editing the corresponding `translation.suml`.
If a locale does not have a value translated, those inside `_base/translation.suml` are used.
Please check `TODO` comments and translation values which have been copied, but not translated from `_base`.
If the original contains internal links, ensure that the route is also valid for your locale.
To check for missing translations, you can use the `admin/translations/missing` route.

### Creating a new language version

Read [this](https://en.pronouns.page/new-version) first. \
When you add a new language, remember to modify the file `./locale/locales.ts` to add your language:
```js
// Add an object according to the following format:
new LocaleDescription('xx', 'xx', 'https://xx.pronouns.page', false, 'x', 'xxx'),

// Setting 'published' to true means that the language was published onto the main site.
// For example:
new LocaleDescription('pt', 'Português', 'https://pt.pronouns.page', true, 'ã', 'romance'),
```

### Current translations being worked on
- Catalan - @peiprjs
- Hebrew - @ThatTransGirl
- [Add yours!]
