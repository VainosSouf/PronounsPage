import { beforeEach, describe, expect, test } from '@jest/globals';

import { Example, ExamplePart, MergedPronounGroup, PronounGroup, PronounLibrary } from '../src/classes.ts';
import { Translator } from '../src/translator.js';
import type { PronounsConfig } from '../locale/config.ts';
import type { Translations } from '../locale/translations.ts';

const translations: Translations = {
    pronouns: {
        any: {
            short: 'any',
        },
        slashes: {
            plural: 'plural',
            pluralHonorific: 'plural-honorific',
            description: 'description',
        },
    },
};
const translator = new Translator(translations, translations, []);

const { default: pronouns, generated: generatedPronouns } = await import('./fixtures/pronouns.ts');

describe('required morphemes for an example', () => {
    const exampleParts = [
        new ExamplePart(false, 'This house is '),
        new ExamplePart(true, 'possessive_pronoun'),
    ];
    const example = new Example(exampleParts, exampleParts);

    test('are present if all morphemes are present', () => {
        expect(example.requiredMorphemesPresent(pronouns.they)).toBe(true);
    });
    test('are present even if one morpheme is empty', () => {
        expect(example.requiredMorphemesPresent(generatedPronouns.aerWithEmptyPossessivePronoun)).toBe(true);
    });
    test('are missing if one morpheme is null', () => {
        expect(example.requiredMorphemesPresent(generatedPronouns.aerWithUnsetPossessivePronoun)).toBe(false);
    });
});

describe('formatting a pronoun with slashes', () => {
    const pronounsConfig: PronounsConfig = {
        route: 'pronouns',
        default: 'he',
        any: 'any',
        plurals: true,
        honorifics: false,
        multiple: {
            name: 'Interchangeable forms',
            description: '…',
            examples: ['he&she'],
        },
        null: false,
        emoji: false,
    };

    describe('when slashes are not configured', () => {
        beforeEach(() => {
            global.config.pronouns = { enabled: true, ...pronounsConfig };
        });
        test('yields no result', () => {
            expect(generatedPronouns.aer.toStringSlashes(translator)).toBeNull();
        });
    });
    describe('when configured that slashes contain all morphemes', () => {
        beforeEach(() => {
            pronounsConfig.slashes = true;
            global.config.pronouns = { enabled: true, ...pronounsConfig };
        });

        test('chunks contain all morphemes', () => {
            expect(generatedPronouns.aer.toStringSlashes(translator))
                .toEqual('ae/aer/aer/aers/aerself');
        });
        test('morphemes with control symbols gets escaped', () => {
            expect(generatedPronouns.sSlashHe.toStringSlashes(translator))
                .toEqual('s`/he/hir/hir/hirs/hirself');
        });
        test('empty morphemes receive space as placeholder', () => {
            expect(generatedPronouns.aerWithEmptyPossessivePronoun.toStringSlashes(translator))
                .toEqual('ae/aer/aer/ /aerself');
        });
        test('empty morphemes at end receive url-encoded space as placeholder', () => {
            expect(generatedPronouns.aerWithEmptyReflexive.toStringSlashes(translator))
                .toEqual('ae/aer/aer/aers/%20');
        });
        test('unset morphemes receive tilde as placeholder', () => {
            expect(generatedPronouns.aerWithUnsetPossessivePronoun.toStringSlashes(translator))
                .toEqual('ae/aer/aer/~/aerself');
        });
        test('adds plural modifier if necessary', () => {
            expect(generatedPronouns.aerPlural.toStringSlashes(translator))
                .toEqual('ae/aer/aer/aers/aerselves/:plural');
        });
        test('adds plural honorific modifier if necessary', () => {
            expect(generatedPronouns.aerPluralHonorific.toStringSlashes(translator))
                .toEqual('ae/aer/aer/aers/aerselves/:plural-honorific');
        });
        test('adds escaped description if necessary', () => {
            expect(generatedPronouns.aerWithDescription.toStringSlashes(translator))
                .toEqual('ae/aer/aer/aers/aerself/:description=Neopronoun “ae” `/ “æ”');
        });
        test('adds multiple modifiers if necessary', () => {
            expect(generatedPronouns.aerPluralWithDescription.toStringSlashes(translator))
                .toEqual('ae/aer/aer/aers/aerselves/:plural/:description=Neopronoun “ae” `/ “æ”');
        });
    });
    describe('when configured that slashes contain some morphemes', () => {
        beforeEach(() => {
            pronounsConfig.slashes = ['pronoun_subject', 'pronoun_object', 'possessive_determiner', 'reflexive'];
            global.config.pronouns = { enabled: true, ...pronounsConfig };
        });

        test('chunks contain configured morphemes', () => {
            expect(generatedPronouns.aer.toStringSlashes(translator)).toEqual('ae/aer/aer/aerself');
        });
    });
});

describe('when merging pronoun groups by key', () => {
    test('groups without keys are not assigned a merged group', () => {
        const groups = [
            new PronounGroup('Normative-ish forms', ['they']),
        ];
        const library = new PronounLibrary(groups, pronouns);
        expect(library.byKey()).toEqual({});
    });

    test('groups with different keys are assigned different merged groups', () => {
        const groups = [
            new PronounGroup('Binary forms', ['he', 'she'], null, 'normative'),
            new PronounGroup('Normative-ish forms', ['they'], null, 'normative-ish'),
        ];
        const library = new PronounLibrary(groups, pronouns);
        expect(library.byKey()).toEqual({
            normative: new MergedPronounGroup('normative', [
                {
                    group: groups[0],
                    groupPronouns: { he: pronouns.he, she: pronouns.she },
                },
            ]),
            'normative-ish': new MergedPronounGroup('normative-ish', [
                {
                    group: groups[1],
                    groupPronouns: { they: pronouns.they },
                },
            ]),
        });
    });

    test('groups with same key are assigned same merged group', () => {
        const groups = [
            new PronounGroup('Binary forms', ['he', 'she'], null, 'normative-ish'),
            new PronounGroup('Normative-ish forms', ['they'], null, 'normative-ish'),
        ];
        const library = new PronounLibrary(groups, pronouns);
        expect(library.byKey()).toEqual({
            'normative-ish': new MergedPronounGroup('normative-ish', [
                {
                    group: groups[0],
                    groupPronouns: { he: pronouns.he, she: pronouns.she },
                },
                {
                    group: groups[1],
                    groupPronouns: { they: pronouns.they },
                },
            ]),
        });
    });
});

describe('when displaying merged groups', () => {
    test('and no group specific translation is available, the key is used', () => {
        const group = new PronounGroup('Binary forms', ['he', 'she'], null, 'normative-ish');
        const merged = new MergedPronounGroup('normative', [
            {
                group,
                groupPronouns: { he: pronouns.he, she: pronouns.she },
            },
        ]);
        expect(merged.short(translator)).toBe('any normative');
    });

    test('and a group specific translation is available, the translation is used', () => {
        translations.pronouns.any.group = {
            normative: {
                short: 'both binaries',
            },
        };

        const group = new PronounGroup('Binary forms', ['he', 'she'], null, 'normative-ish');
        const merged = new MergedPronounGroup('normative', [
            {
                group,
                groupPronouns: { he: pronouns.he, she: pronouns.she },
            },
        ]);
        expect(merged.short(translator)).toBe('both binaries');
    });
});
