import { describe, expect, test } from '@jest/globals';

import allLocales from '../../locale/locales.ts';
import { loadTsv } from '../../src/tsv.js';

const __dirname = new URL('.', import.meta.url).pathname;

describe.each(allLocales)('data files of $code', ({ code }) => {
    test('pronouns.tsv match schema', async () => {
        const { default: MORPHEMES } = await import(`../../locale/${code}/pronouns/morphemes.js`);
        const pronouns = loadTsv(`${__dirname}/../../locale/${code}/pronouns/pronouns.tsv`);
        if (pronouns.length === 0) {
            return;
        }
        const required = [
            'key',
            'description',
            'normative',
            'plural',
            'pluralHonorific',
            'pronounceable',
            ...MORPHEMES,
        ];
        const optional = ['history', 'thirdForm', 'smallForm', 'sourcesInfo'];
        const actual = Object.keys(pronouns[0]);
        expect(actual).toEqual(expect.arrayContaining(required));
        expect([...required, ...optional]).toEqual(expect.arrayContaining(actual));
    });
});
