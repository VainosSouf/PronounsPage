import { afterEach, beforeEach, describe, expect, jest, test } from '@jest/globals';
import pathToRegexp from 'path-to-regexp';
import type { ExpectationResult, MatcherUtils } from 'expect';

import { loadSumlFromBase } from '../../server/loader.ts';
import allLocales from '../../locale/locales.ts';
import { deepGet, deepListKeys } from '../../src/helpers.ts';
import type { Translations } from '../../locale/translations.ts';
import type { NuxtConfig } from '@nuxt/types';
import type { NuxtRouteConfig } from '@nuxt/types/config/router';

const baseTranslations = loadSumlFromBase('locale/_base/translations') as Translations;
const typeFlexibleKeys = new Set(['home.generator.alt']);

declare module 'expect' {
    interface Matchers<R> {
        toMatchBaseTranslationSchema(): R;
        toBeValidPath(paths: RegExp[], key: string): R;
    }
}

function specificTypeOf(value: unknown): string | null {
    if (typeof value === 'object') {
        if (value === null) {
            return null;
        } else if (Array.isArray(value)) {
            return 'array';
        } else {
            return 'object';
        }
    } else {
        return typeof value;
    }
}

function toMatchBaseTranslationSchema(actual: Translations): ExpectationResult {
    const messages = recursivelyValidateSchema(actual, baseTranslations);
    if (messages.length > 0) {
        return {
            message: () => `expected translations to match schema of base translations\n\n${messages.join('\n')}`,
            pass: false,
        };
    } else {
        return {
            message: () => 'expected translations to mismatch schema of base translations',
            pass: true,
        };
    }
}

function recursivelyValidateSchema(actual: Translations, base: Translations, parentKey: string = ''): string[] {
    const messages = [];
    for (const [property, value] of Object.entries(actual)) {
        const key = parentKey ? `${parentKey}.${property}` : property;
        if (base[property] === undefined || base[property] === null) {
            continue;
        }
        if (value !== null && !typeFlexibleKeys.has(key) && specificTypeOf(value) !== specificTypeOf(base[property])) {
            messages.push(`${key} has type ${specificTypeOf(value)}, expected ${specificTypeOf(base[property])}`);
        }
        if (specificTypeOf(value) === 'object') {
            messages.push(...recursivelyValidateSchema(value, base[property], key));
        }
    }
    return messages;
}

function toBeValidPath(this: MatcherUtils, actual: string, paths: RegExp[], key: string): ExpectationResult {
    const encoded = encodeURI(actual);
    const pass = paths.some((matcher) => encoded.match(matcher));
    if (pass) {
        return {
            message: () => `expected ${this.utils.printReceived(actual)} inside ${this.utils.printReceived(key)} ` +
                'to not be a known internal link',
            pass: true,
        };
    } else {
        return {
            message: () => `expected ${this.utils.printReceived(actual)} inside ${this.utils.printReceived(key)} ` +
                'to be a known internal link',
            pass: false,
        };
    }
}

const pathRegex = /\{(\/[^}]+?)(?:#[^}]+)?=[^}]+\}/g;

describe.each(allLocales)('translations for $code', ({ code, published }) => {
    const config = loadSumlFromBase(`locale/${code}/config`);
    const translations = loadSumlFromBase(`locale/${code}/translations`) as Translations;

    beforeEach(() => {
        jest.unstable_mockModule('../../server/loader.js', () => {
            return {
                loadSuml(name: string): unknown {
                    if (name === 'config') {
                        return config;
                    } else if (name === 'translations') {
                        return translations;
                    }
                },
            };
        });
    });

    afterEach(() => {
        jest.resetModules();
    });

    test('match schema of base translations', () => {
        expect(translations).toMatchBaseTranslationSchema();
    });

    test('contain valid internal links', async () => {
        if (!published) {
            // unpublished versions are ignored for now because they have a lot of errors
            return;
        }

        // use query string to force reload
        const { default: nuxtConfig }: { default: NuxtConfig } = await import(`../../nuxt.config.js?${code}`);

        const routes: NuxtRouteConfig[] = [];
        nuxtConfig.router!.extendRoutes!(routes, () => '');
        const paths = routes.filter((route) => route.name !== 'all').map((route) => pathToRegexp(route.path));

        for (const key of deepListKeys(translations)) {
            const translation = deepGet(translations, key);
            if (typeof translation !== 'string') {
                continue;
            }
            for (const match of translation.matchAll(pathRegex)) {
                expect(match[1]).toBeValidPath(paths, key);
            }
        }
    });
});

expect.extend({ toMatchBaseTranslationSchema, toBeValidPath });
