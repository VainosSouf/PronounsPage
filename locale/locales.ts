export class LocaleDescription {
    code: string;
    name: string;
    url: string;
    published: boolean;
    symbol: string;
    family: string;
    extra: string | null;

    constructor(
        code: string,
        name: string,
        url: string,
        published: boolean,
        symbol: string,
        family: string,
        extra: string | null = null,
    ) {
        this.code = code;
        this.name = name;
        this.url = url;
        this.published = published;
        this.symbol = symbol;
        this.family = family;
        this.extra = extra;
    }
}

export default [
    new LocaleDescription('de', 'Deutsch', 'https://de.pronouns.page', true, 'ß', 'germanic'),
    new LocaleDescription('es', 'Español', 'https://pronombr.es', true, 'ñ', 'romance'),
    new LocaleDescription('eo', 'Esperanto', 'https://eo.pronouns.page', false, 'ĥ', 'constructed'),
    new LocaleDescription('en', 'English', 'https://en.pronouns.page', true, 'þ', 'germanic'),
    new LocaleDescription('et', 'Eesti keel', 'https://et.pronouns.page', true, 'õ', 'finnish'),
    new LocaleDescription('fr', 'Français', 'https://pronoms.fr', true, 'ç', 'romance'),
    // symbol duplicate with spanish
    new LocaleDescription('gl', 'Galego', 'https://gl.pronouns.page', false, 'ñ', 'romance'),
    // not entirely sure about the languange family
    new LocaleDescription('he', 'עברית', 'https://he.pronouns.page', false, 'ע', 'semitic'),
    new LocaleDescription('it', 'Italiano', 'https://it.pronouns.page', false, 'à', 'romance'),
    new LocaleDescription('lad', 'Ladino', 'https://lad.pronouns.page', true, 'ny', 'romance', 'Djudezmo'),
    new LocaleDescription('nl', 'Nederlands', 'https://nl.pronouns.page', true, 'ĳ', 'germanic'),
    // å might be better, but it's used for swedish
    new LocaleDescription('no', 'Norsk', 'https://no.pronouns.page', true, 'æ', 'germanic', 'Bokmål'),
    new LocaleDescription('pl', 'Polski', 'https://zaimki.pl', true, 'ą', 'slavic'),
    new LocaleDescription('pt', 'Português', 'https://pt.pronouns.page', true, 'ã', 'romance'),
    new LocaleDescription('ro', 'Română', 'https://ro.pronouns.page', true, 'ă', 'romance'),
    new LocaleDescription('sv', 'Svenska', 'https://sv.pronouns.page', true, 'å', 'germanic'),
    new LocaleDescription('tr', 'Türkçe', 'https://tr.pronouns.page', false, 'ş', 'turkic'),
    new LocaleDescription('vi', 'Tiếng Việt', 'https://vi.pronouns.page', true, 'ớ', 'vietic'),
    new LocaleDescription('ar', 'العربية', 'https://ar.pronouns.page', false, 'ش', 'semitic', 'الفصحى'),
    new LocaleDescription('ru', 'Русский', 'https://ru.pronouns.page', true, 'й', 'slavic'),
    new LocaleDescription('ua', 'Українська', 'https://ua.pronouns.page', true, 'ї', 'slavic'),
    new LocaleDescription('ja', '日本語', 'https://ja.pronouns.page', true, 'の', 'japonic'),
    new LocaleDescription('ko', '한국어', 'https://ko.pronouns.page', false, '인', 'koreanic'),
    new LocaleDescription('yi', 'ייִדיש', 'https://yi.pronouns.page', false, 'ש', 'germanic'),
    new LocaleDescription('zh', '中文', 'https://zh.pronouns.page', true, '人', ''),
    new LocaleDescription('tok', 'toki pona', 'https://tok.pronouns.page', false, '⊡', 'constructed'),
];
