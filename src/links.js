import AbortController from 'abort-controller';
import fetch from 'node-fetch';
import { JSDOM } from 'jsdom';
import VirtualConsole from 'jsdom/lib/jsdom/virtual-console.js';

export const normaliseUrl = (url) => {
    try {
        return new URL(url).toString();
    } catch {
        return null;
    }
};

export class LinkAnalyser {
    async analyse(url) {
        url = new URL(url);
        let $document;
        try {
            const controller = new AbortController();
            setTimeout(() => controller.abort(), 10000);
            const htmlString = await (await fetch(url, { signal: controller.signal })).text();
            $document = new JSDOM(htmlString, { virtualConsole: new VirtualConsole() });
        } catch (e) {
            return {
                url: url.toString(),
                error: e,
            };
        }

        const favicon = await this._findFavicon(url, $document);

        return {
            url: url.toString(),
            relMe: await this._findRelMe($document),
            favicon: favicon ? favicon.toString() : null,
            nodeinfo: await this._fetchNodeInfo(url),
        };
    }

    async _findRelMe($document) {
        const links = new Set();
        for (const $el of $document.window.document.querySelectorAll('[rel=me]')) {
            if (!['A', 'LINK'].includes($el.tagName)) {
                continue;
            }
            const link = ($el.attributes.value || $el.attributes.href)?.value;
            if (!link) {
                continue;
            }

            links.add(link);
        }
        return [...links];
    }

    async _findFavicon(url, $document) {
        for (const $el of $document.window.document.querySelectorAll('link[rel="icon"], link[rel="shortcut icon"], link[rel="mask-icon"]')) {
            const link = $el.attributes.href?.value;
            if (!link) {
                continue;
            }

            return new URL(link, url);
        }

        try {
            const fallback = new URL('/favicon.ico', url);
            const controller = new AbortController();
            setTimeout(() => controller.abort(), 1000);
            const res = await fetch(fallback, { signal: controller.signal });
            if (res.ok) {
                return fallback;
            }
        } catch {}

        return null;
    }

    async _fetchNodeInfo(url) {
        try {
            const res = await fetch(new URL('/.well-known/nodeinfo', url));
            if (res.status !== 200) {
                return null;
            }
            const link = (await res.json())?.links?.[0]?.href;
            if (!link) {
                return null;
            }

            return await (await fetch(new URL(link, url))).json();
        } catch {
            return null;
        }
    }
}
