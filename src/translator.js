import * as Sentry from '@sentry/browser';

import { deepGet } from './helpers.ts';
import { listMissingTranslations } from './missingTranslations.ts';

export class Translator {
    constructor(translations, baseTranslations, config) {
        this.translations = translations;
        this.baseTranslations = baseTranslations;
        this.config = config;
    }

    translate(key, params = {}, warn = false) {
        return this.applyParams(
            this.get(key, warn),
            params,
        );
    }

    get(key, warn = false, base = false, useFallback = true) {
        const translations = base ? this.baseTranslations : this.translations;
        const value = deepGet(translations, key);
        if (value === undefined) {
            if (warn) {
                Sentry.captureMessage(`Cannot find translation: ${key}`, 'warning');
            }
            if (!base && useFallback) {
                return this.get(key, warn, true);
            }
        }
        return value;
    }

    has(key) {
        return this.get(key, false, false, false) !== undefined;
    }

    hasFallback(key) {
        return this.get(key, false, true, false) !== undefined;
    }

    applyParams(value, params = {}) {
        if (!value) {
            return value;
        }
        for (const k in params) {
            if (params.hasOwnProperty(k)) {
                value = Array.isArray(value)
                    ? value.map((v) => v.replace(new RegExp(`%${k}%`, 'g'), params[k]))
                    : value.replace(new RegExp(`%${k}%`, 'g'), params[k]);
            }
        }
        return value;
    }

    listMissingTranslations() {
        return listMissingTranslations(this.translations, this.baseTranslations, this.config);
    }
}
