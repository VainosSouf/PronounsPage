export interface User {
    id: string;
    username: string;
    email: string;
    emailHash?: string;
    roles: string;
    avatarSource: string;
    bannedReason: string;
    bannedBy: string;
    lastActive: number;
    banSnapshot: string;
    inactiveWarning: number;
    adminNotifications: number;
    loginAttempts: string;
    timesheets: string;
    socialLookup: number;
    payload?: string;
    mfa?: boolean;
    mfaRequired?: boolean;
    authenticated?: boolean;
}
