import md5 from 'js-md5';
import { v5 as uuid5 } from 'uuid';
import nepali from 'nepali-calendar-js';

export class Day {
    constructor(year, month, day, dayOfWeek) {
        this.year = parseInt(year);
        this.month = parseInt(month);
        this.day = parseInt(day);
        this.dayOfWeek = dayOfWeek ? parseInt(dayOfWeek) : new Date(this.year, this.month - 1, this.day).getDay() || 7;
    }

    static fromDate(date) {
        return new Day(date.getFullYear(), date.getMonth() + 1, date.getDate());
    }

    static today() {
        return Day.fromDate(new Date());
    }

    toDate() {
        return new Date(this.year, this.month - 1, this.day);
    }

    equals(other) {
        return other && this.year === other.year && this.month === other.month && this.day === other.day;
    }

    toString() {
        return `${this.year}-${this.month.toString().padStart(2, '0')}-${this.day.toString().padStart(2, '0')}`;
    }

    // for comparisons
    toInt() {
        return parseInt(`${this.year}${this.month.toString().padStart(2, '0')}${this.day.toString().padStart(2, '0')}`);
    }

    next() {
        const d = this.toDate();
        d.setDate(d.getDate() + 1);
        return Day.fromDate(d);
    }

    prev() {
        const d = this.toDate();
        d.setDate(d.getDate() - 1);
        return Day.fromDate(d);
    }
}

export function* iterateMonth(year, month) {
    for (let day = 1; day <= 31; day++) {
        const d = new Date(year, month - 1, day);
        if (d.getDate() !== day) {
            return;
        }
        yield new Day(year, month, day, d.getDay() || 7);
    }
}

export const EventLevel = {
    Month: 0,
    Week: 1,
    Nameday: 2,
    Day: 3,
    CustomDay: 4,
};

export class Event {
    constructor(name, flag, month, generator, level, terms = [], timeDescription = null, localCalendar = null, yearCondition = null) {
        this.name = name;
        this.flag = flag;
        this.month = month;
        this.generator = generator;
        this.level = level;
        this.terms = terms;
        this.timeDescription = timeDescription;
        this.localCalendar = localCalendar;
        this.yearCondition = yearCondition;
        this.daysMemoise = {};
    }

    getDays(year) {
        year = parseInt(year);

        if (this.yearCondition && !this.yearCondition(year)) {
            return [];
        }

        if (this.daysMemoise === undefined) {
            // shouldn't happen, but somehow does, but only on prod?
            this.daysMemoise = {};
        }

        if (this.daysMemoise[year] === undefined) {
            this.daysMemoise[year] = [...this.generator(iterateMonth(year, this.month))];
        }

        return this.daysMemoise[year];
    }

    length() {
        return [...this.getDays(2021)].length;
    }

    getRange(year) {
        if (year === undefined) {
            year = Day.today().year;
        }
        const days = this.getDays(year);
        if (days.length === 1) {
            return days[0].day;
        }

        return `${days[0].day} – ${days[days.length - 1].day}`;
    }

    isFirstDay(day) {
        return this.getDays(day.year)[0].equals(day);
    }

    getUuid() {
        return uuid5(`${process.env.BASE_URL}/calendar/event/${this.name}`, uuid5.URL);
    }

    toIcs(year, translations, fallbackTranslations, clearLinkedText, sequence = 1, onlyFirstDays = false, calNameExtra = '') {
        const days = this.getDays(year);
        if (!days.length) {
            return null;
        }

        let [name, param] = this.name.split('$');
        if (translations.calendar.events[name] !== undefined) {
            name = translations.calendar.events[name];
        } else if (fallbackTranslations.calendar.events[name] !== undefined) {
            name = fallbackTranslations.calendar.events[name];
        }
        if (param) {
            name = name.replace(/%param%/g, param);
        }
        name = clearLinkedText(name);

        const first = days[0];
        let last = days[days.length - 1];
        if (onlyFirstDays && !first.equals(last)) {
            last = first;
            name += ` (${translations.calendar.start || fallbackTranslations.calendar.start})`;
        }
        last = last.next();

        return {
            title: name,
            start: [first.year, first.month, first.day],
            end: [last.year, last.month, last.day],
            calName: translations.calendar.headerLong + calNameExtra,
            sequence,
        };
    }

    static fromCustom(customSettings) {
        return new Event(
            customSettings.name,
            null,
            customSettings.month,
            day(customSettings.day),
            EventLevel.CustomDay,
        );
    }
}

class NepaliDay extends Day {
    constructor(gYear, gMonth, gDay, gDayOfWeek, nYear, nMonth, nDay) {
        super(gYear, gMonth, gDay, gDayOfWeek);
        this.nYear = nYear;
        this.nMonth = nMonth;
        this.nDay = nDay;
    }
}

export class NepaliEvent extends Event {
    getDays(year) {
        year = parseInt(year);

        if (this.daysMemoise === undefined) {
            // shouldn't happen, but somehow does, but only on prod?
            this.daysMemoise = {};
        }

        if (this.daysMemoise[year] === undefined) {
            this.daysMemoise[year] = [...this.generator(this._iterateNepaliMonth(year, this.month))];
        }

        return this.daysMemoise[year];
    }

    *_iterateNepaliMonth(gYear, nMonth) {
        for (const possibleYearOffset of [56, 57]) {
            const daysInMonth = nepali.nepaliMonthLength(gYear + possibleYearOffset, nMonth);
            for (let nDay = 1; nDay <= daysInMonth; nDay++) {
                const { gy, gm, gd } = nepali.toGregorian(gYear + possibleYearOffset, nMonth, nDay);
                if (gy === gYear) {
                    yield new NepaliDay(gy, gm, gd, null, gYear + possibleYearOffset, nMonth, nDay);
                }
            }
        }
    }
}

export function day(dayOfMonth) {
    function *internal(monthDays) {
        for (const d of monthDays) {
            if (d.day === dayOfMonth) {
                yield d;
            }
        }
    }

    return internal;
}

export function* month(monthDays) {
    for (const d of monthDays) {
        yield d;
    }
}

export function week(generator) {
    function *internal(monthDays) {
        let count = 0;
        for (const d of generator(monthDays)) {
            yield d;
            count++;
            if (count === 7) {
                return;
            }
        }
    }

    return internal;
}

export function weekStarting(start) {
    function *internal(monthDays) {
        let count = 0;
        for (const d of monthDays) {
            if (d.day >= start && count < 7) {
                yield d;
                count++;
            }
        }
    }

    return internal;
}

export function dayYear(day, year) {
    function *internal(monthDays) {
        for (const d of monthDays) {
            if (d.day === day && d.year === year) {
                yield d;
            }
        }
    }

    return internal;
}

export class Year {
    constructor(year, events) {
        this.year = year;
        this.events = events;

        this.eventsByDate = {};
        for (const event of events) {
            for (const d of event.getDays(year)) {
                const k = d.toString();
                if (this.eventsByDate[k] === undefined) {
                    this.eventsByDate[k] = [];
                }
                this.eventsByDate[k].push(event);
            }
        }
        for (const date in this.eventsByDate) {
            if (!this.eventsByDate.hasOwnProperty(date)) {
                continue;
            }
            this.eventsByDate[date].sort((a, b) => b.level - a.level);
        }

        this.eventsByTerm = {};
        for (const event of events) {
            for (const term of event.terms) {
                if (this.eventsByTerm[term] === undefined) {
                    this.eventsByTerm[term] = [];
                }
                if (event.getDays(this.year).length) {
                    this.eventsByTerm[term].push(event);
                }
            }
        }
        for (const term in this.eventsByTerm) {
            if (!this.eventsByTerm.hasOwnProperty(term)) {
                continue;
            }
            this.eventsByTerm[term].sort((a, b) => a.getDays(this.year)[0].toInt() - b.getDays(this.year)[0].toInt());
        }

        this.eventsByUuid = {};
        for (const event of events) {
            this.eventsByUuid[event.getUuid()] = event;
        }

        this.eventsByName = {};
        for (const event of events) {
            this.eventsByName[event.name] = event;
        }
    }

    isCurrent() {
        return this.year === Day.today().year;
    }
}

export class Calendar {
    constructor(events, minYear = 0, maxYear = 9999) {
        this._events = events;
        this._minYear = minYear;
        this._maxYear = maxYear;
        this._years = {};
    }

    getYear(year) {
        year = parseInt(year);
        if (year < this._minYear || year > this._maxYear || Number.isNaN(year)) {
            return null;
        }

        if (this._years[year] === undefined) {
            this._years[year] = new Year(year, this._events);
        }

        return this._years[year];
    }

    getCurrentYear() {
        return this.getYear(Day.today().year);
    }

    *getAllYears() {
        for (let y = this._minYear; y <= this._maxYear; y++) {
            yield this.getYear(y);
        }
    }

    buildSummary() {
        const summary = {};
        for (const year of this.getAllYears()) {
            for (let month = 1; month <= 12; month++) {
                for (const day of iterateMonth(year.year, month)) {
                    const events = [];
                    for (const event of year.eventsByDate[day.toString()] || []) {
                        events.push(event.name);
                    }
                    summary[day.toString()] = md5(JSON.stringify(events));
                }
            }
        }
        return summary;
    }

    static generatePersonalCalendarEvents(events, year) {
        return [...new Set(events)]
            .map((event) => typeof event === 'string'
                ? year.eventsByName[event]
                : Event.fromCustom(event))
            .filter((e) => !!e)
            .sort((a, b) => {
                const aFirstDay = a.getDays(year.year)[0];
                const bFirstDay = b.getDays(year.year)[0];

                const monthDiff = aFirstDay.month - bFirstDay.month;
                if (monthDiff !== 0) {
                    return monthDiff;
                }

                return aFirstDay.day - bFirstDay.day;
            });
    }
}
