import { createRequire } from 'module';

import { Calendar, Event, EventLevel, day } from './helpers.js';
import internationalEvents from '../../locale/_/calendar/events.js';
import localEvents from '../../data/calendar/events.js';

const require = createRequire(import.meta.url);
const rawNamedays = require('../../data/names/namedays.json');

const namedays = [];
for (const name in rawNamedays) {
    if (!rawNamedays.hasOwnProperty(name)) {
        continue;
    }
    for (const nd of rawNamedays[name]) {
        const [m, d] = nd.split('-');
        namedays.push(new Event(`nameday$${name}`, null, parseInt(m), day(parseInt(d)), EventLevel.Nameday));
    }
}

export const calendar = new Calendar(
    [...internationalEvents, ...localEvents], // TODO , ...namedays
    2021,
    2024,
);
