import Vue from 'vue';

export type Mode = 'light' | 'dark' | 'automatic';

export default Vue.extend({
    methods: {
        getMode(): Mode {
            if (!process.client) {
                return 'automatic';
            }

            return localStorage.getItem('mode') as Mode | null || 'automatic';
        },
        detectDark(): boolean {
            if (!process.client) {
                return false;
            }

            switch (this.getMode()) {
                case 'light':
                    return false;
                case 'dark':
                    return true;
                case 'automatic':
                default:
                    return window.matchMedia('(prefers-color-scheme: dark)').matches;
            }
        },
        setMode(mode: Mode): void {
            if (!process.client) {
                return;
            }

            localStorage.setItem('mode', mode);
        },
        setIsDark(dark: boolean): void {
            if (!process.client) {
                return;
            }

            if (dark) {
                document.body.setAttribute('data-theme', 'dark');
                document.body.setAttribute('data-bs-theme', 'dark');
            } else {
                document.body.removeAttribute('data-theme');
                document.body.removeAttribute('data-bs-theme');
            }
        },
    },
});
