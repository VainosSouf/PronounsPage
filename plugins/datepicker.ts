import Vue from 'vue';
import VuejsDatePicker from 'vuejs-datepicker';
import type { Plugin } from '@nuxt/types';

const plugin: Plugin = () => {
    Vue.component('Datepicker', VuejsDatePicker);
};

export default plugin;
