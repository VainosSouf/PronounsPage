import type { Plugin } from '@nuxt/types';

const plugin: Plugin = ({ $axios, app }) => {
    $axios.onRequest((config) => {
        const token = app.$csrfToken();

        if (!config.headers['X-CSRF-Token'] && token) {
            config.headers['X-CSRF-Token'] = token;
        }

        return config;
    });
};

export default plugin;
