import Vue from 'vue';
import type { Plugin } from '@nuxt/types';
import { Translator } from '../src/translator.js';
import { buildDict } from '../src/helpers.ts';
import { DateTime, Settings } from 'luxon';
import { decodeTime } from 'ulid';
import type { Pronoun } from '../src/classes.ts';
import type { LocaleDescription } from '../locale/locales.ts';

import translations from '../data/translations.suml';
import baseTranslations from '../locale/_base/translations.suml';
import { LoadScriptError } from '../src/errors.ts';
import type { Config } from '../locale/config.ts';

declare global {
    interface Window {
        fusetag: any;
    }
}

declare module '@nuxt/types/config/runtime' {
    interface NuxtRuntimeConfig extends Config { }
}

declare module 'vue/types/vue' {
    interface Vue {
        $eventHub: Vue;
        $base: string;
        $translator: Translator;
        $t(key: string, params?: Record<string, string>, warn?: boolean): string;
        $te(key: string, fallback?: boolean): boolean;
        $translateForPronoun(str: string, pronoun: Pronoun): string;
        $locales: Record<string, LocaleDescription>;
        $loadScript(name: string, src: string): Promise<unknown>;
    }
}

const plugin: Plugin = ({ app, store }) => {
    Vue.prototype.$eventHub = new Vue();

    Vue.prototype.$base = process.env.BASE_URL;

    const translator = new Translator(translations, baseTranslations, app.$config);
    Vue.prototype.$translator = translator;
    app.$translator = translator; // it should be accessible like this from the previous line, but for some reason it's not, hence this ugly workaround
    Vue.prototype.$t = (key: string, params = {}, warn = false): string => translator.translate(key, params, warn);
    Vue.prototype.$te = (key: string, fallback = false): boolean => {
        if (translator.has(key)) {
            return true;
        }
        if (fallback && translator.hasFallback(key)) {
            return true;
        }
        return false;
    };
    Vue.prototype.$translateForPronoun = (str: string, pronoun: Pronoun): string => {
        let translation = translator.translate(`flags.${str.replace(/ /g, '_').replace(/'/g, '*')}`, {}, false) || str;
        if (pronoun) {
            translation = pronoun.format(translation);
        }
        return translation;
    };

    Vue.prototype.$locales = buildDict(function* () {
        const locales = process.env.LOCALES as unknown as Record<string, LocaleDescription>;
        if (app.$config.locale !== '_') {
            yield [app.$config.locale, locales[app.$config.locale]];
        }
        for (const [locale, localeDescription] of Object.entries(locales)) {
            if (locale !== app.$config.locale) {
                yield [locale, localeDescription];
            }
        }
    });

    store.commit('setSpelling', app.$cookies.get('spelling'));
    store.commit('restoreTranslations', app.$cookies.get('translations'));
    if (app.$cookies.get('translationModeVisible')) {
        store.commit('showTranslationMode');
    }

    Vue.prototype.$loadScript = (name: string, src: string, nonce: string | undefined = undefined): Promise<void> => {
        if (!process.client || document.querySelectorAll(`script.${name}-script`).length > 0) {
            return new Promise((resolve) => {
                resolve();
            });
        }

        return new Promise((resolve, reject) => {
            const script = document.createElement('script');
            script.setAttribute('src', src);
            if (nonce) {
                script.setAttribute('nonce', nonce);
            }
            script.classList.add(`${name}-script`);
            // script.crossOrigin = 'true'; TODO proper fix (adding it breaks publift)
            script.addEventListener('load', () => resolve());
            script.addEventListener('error', (event) => {
                reject(new LoadScriptError(name, src, typeof event === 'string' ? event : event.type));
            });
            document.body.appendChild(script);
        });
    };

    try {
        Settings.defaultLocale = app.$config.intlLocale || app.$config.locale;
        DateTime.now().toFormat('y-MM-dd HH:mm'); // test if locale is supported by luxon
    } catch {
        Settings.defaultLocale = 'en';
    }

    Vue.prototype.$datetime = (timestamp: number): string => {
        const dt = DateTime.fromSeconds(timestamp);
        return dt.toFormat('y-MM-dd HH:mm');
    };

    Vue.prototype.$ulidTime = (ulid: string): number => {
        return decodeTime(ulid) / 1000;
    };

    app.router?.afterEach(() => {
        if (typeof window !== 'undefined' && window.fusetag && window.fusetag.pageInit) {
            window.fusetag.pageInit();
        }
    });
};

export default plugin;
