import type { Plugin } from '@nuxt/types';

const plugin: Plugin = () => {
    if (!Object.prototype.hasOwnProperty.call(Object, 'hasOwn')) {
        Object.defineProperty(Object, 'hasOwn', {
            value: (object: object, property: PropertyKey) => {
                return Object.prototype.hasOwnProperty.call(object, property);
            },
        });
    }
};

export default plugin;
