import { mapState } from 'vuex';
import zhConverter from 'zh_cn_zh_tw';
import futurus from 'avris-futurus';
import { escapeHtml } from '../src/helpers.ts';

export default {
    computed: {
        ...mapState([
            'spelling',
        ]),
    },
    methods: {
        handleSpelling(str) {
            if (this.$config.locale === 'zh' && this.spelling === 'simplified') {
                return zhConverter.convertToSimplifiedChinese(str);
            }

            if (this.$config.locale === 'pl' && this.spelling === 'futurysci') {
                return futurus.futuriseText(str);
            }

            if (this.$config.locale === 'en' && this.spelling === 'shavian') {
                if (typeof window === 'undefined' || !window.toShavian) {
                    this.setSpelling(''); // dependencies not loaded, disable
                    return str;
                }

                const shavian = str.replace(/(<[^>]*>)|([^<]+)/g, (match, htmlTag, text) => {
                    if (htmlTag) {
                        return htmlTag;
                    }

                    return window.toShavian(text);
                });

                return `<span class="shavian">${shavian}</span>`;
            }

            if (this.$config.locale === 'tok' && this.spelling === 'sitelen') {
                return `<span class="sitelen">${str}</span>`;
            }

            return str;
        },
        convertName(name) {
            if (this.$config.locale === 'tok') {
                const m = name.match(/^jan (.+?) \(((?:[mnptkswlj]?[iueoa][mn]? ?)+)\)$/i);
                if (!m) {
                    return escapeHtml(name);
                }

                if (this.spelling === 'sitelen') {
                    return `jan <span class="cartouche">${escapeHtml(m[2])}</span>`;
                }

                return `jan ${escapeHtml(m[1])}`;
            }

            return escapeHtml(name);
        },
        setSpelling(spelling) {
            this.$store.commit('setSpelling', spelling);
            this.$cookies.set('spelling', this.$store.state.spelling);
        },
    },
};
