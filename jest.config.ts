import type { Config } from 'jest';

const config: Config = {
    extensionsToTreatAsEsm: ['.ts'],
    globals: {
        // define the config global here, but fill its properties later in the individual tests
        config: {},
    },
    moduleNameMapper: {
        '^(\\.{1,2}/.*)\\.js$': '$1',
    },
    testEnvironment: 'node',
    testPathIgnorePatterns: ['.yarn', 'node_modules'],
    transform: {
        '^.+\\.ts$': ['ts-jest', {
            // use isolated modules to speed up test execution; type checking happens separately
            isolatedModules: true,
            useESM: true,
        }],
    },
};

export default config;
