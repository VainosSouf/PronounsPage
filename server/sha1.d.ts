declare module 'sha1' {
    const sha1: (message: string) => string;
    export = sha1;
}
