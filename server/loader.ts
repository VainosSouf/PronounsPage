import fs from 'fs';
import Suml from 'suml';
import Papa from 'papaparse';

export const loadSumlFromBase = (name: string): unknown => new Suml().parse(fs.readFileSync(`./${name}.suml`, 'utf-8'));
export const loadSuml = (name: string): unknown => loadSumlFromBase(`data/${name}`);

export const loadTsv = (name: string): unknown => Papa.parse(fs.readFileSync(`./data/${name}.tsv`).toString(), {
    dynamicTyping: true,
    header: true,
    skipEmptyLines: true,
    delimiter: '\t',
}).data;
