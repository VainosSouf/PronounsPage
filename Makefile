include .env.dist
-include .env

KEYS_DIR=./keys

# make targets phony because some commands match with a directory
.PHONY: install test run start stop deploy switch migrate

install:
	-cp -n .env.dist .env
	if [ ! -d "${KEYS_DIR}" ]; then mkdir -p ${KEYS_DIR}; openssl genrsa  -out ${KEYS_DIR}/private.pem 2048; openssl rsa -in ${KEYS_DIR}/private.pem -outform PEM -pubout -out ${KEYS_DIR}/public.pem; fi
	mkdir -p moderation
	touch moderation/sus.txt moderation/rules-users.md moderation/rules-terminology.md moderation/rules-sources.md
	yarn
	yarn ts-node server/migrate.ts
	yarn ts-node locale/generateSchemas.ts

lint:
	yarn lint

test:
	yarn test

run:
	yarn dev

start:
	node_modules/.bin/avris-daemonise start webserver yarn dev

stop:
	node_modules/.bin/avris-daemonise stop webserver

deploy: install
	mkdir -p ./cache
	git log -n 1 --pretty=format:"%H" > ./cache/version
	yarn build
	yarn ts-node server/migrate.ts
	ln -sfn ../data/img ./static/img-local
	ln -sfn ../data/docs ./static/docs-local
	yarn ts-node server/sentry.ts

switch:
	rm -rf cache
	ln -sfn ./locale/${LANG} ./data
	ln -sfn ../locale/${LANG}/img ./static/img-local
	ln -sfn ../locale/${LANG}/docs ./static/docs-local
	if [ -d ./locale/${LANG}/img/logo ]; then echo ""; else ln -s ../../_/img/logo ./locale/${LANG}/img/logo; fi

migrate:
	yarn ts-node server/migrate.ts
